"""
Produces requested output
Author: William A
April 2022
"""

import logging
import pprint


logging.basicConfig(level=logging.INFO, format="%(asctime)-15s %(message)s")
logger = logging.getLogger()


def output_process(flights):
    """Takes input data structure and creates the requested putput as per
    https://github.com/kiwicom/python-weekend-entry-task"""
    all_results_formatted = []
    for each_dict in flights:
        result = {"flights": each_dict[:2]}
        result = {**result, **each_dict[-1]}
        all_results_formatted.append(result)

    logger.info("SUCCESS: Generated json-compatible structured list with flights ")

    return all_results_formatted





