from nonstop import search_nonstop
from stopovers import search_stopovers
from calculator import calculate
from output import output_process
import configparser
import argparse
import pprint
import json


def get_args():
    """
    Creates argument parser for Flight Search Script

    :return: parser
    """

    parser = argparse.ArgumentParser(description="Takes origin, destination and bags to return flights options")
    parser.add_argument('filename')
    parser.add_argument("--origin", type=str, required=True, help="Code name for origin airport")
    parser.add_argument("--destination", type=str, required=True, help="Code name for destination airport")
    parser.add_argument("--bags", default=0, type=int, required=False, help="number of bags")
    parser.add_argument("--return", default=False, type=bool, required=False, help="True when is a Roundtrip")

    return parser.parse_args()


def main():
    """
       Creates argument parser for Flight Search Script

       :return: parser
       """

    config = configparser.ConfigParser()
    config.read("config.ini")

    args = get_args()
    origin = args.origin
    destination = args.destination
    bags = args.bags
    file = args.filename

    search_nonstop(origin, destination, bags, file) #pending to add the output of direct flight
    flight_results = search_stopovers(origin, destination, bags, file)
    total_cost = calculate(flight_results, origin, destination, bags)
    output = output_process(total_cost)
    print("total cost: ", "\n", json.dumps(output, indent=2))

    print(args)


if __name__ == "__main__":
    main()