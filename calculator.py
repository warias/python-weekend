"""
Calculates total cost of each flight
Taking into account Base price, bag price and number of them
Author: William A
April 2022
"""

import logging


logging.basicConfig(level=logging.INFO, format="%(asctime)-15s %(message)s")
logger = logging.getLogger()


def calculate(flights, origin, destination, bags):
    """Takes input arguments plus flights information
        and returns a list with the total cost of the flights"""

    trips_list = flights['flights']
    trip_list_totals = []

    trips = [[trips_list[0], trips_list[i]] for i in range(1, len(trips_list))]
    for i, trip in enumerate(trips):
        totals = {"bags_allowed": 0,
                  "bags_count": 0,
                  "destination": "",
                  "origin": "",
                  "total_price": 0.0,
                  "travel_time": ""}

        totals["bags_allowed"] = min(
            int(trip[0]["bags_allowed"]), int(trip[1]["bags_allowed"]))
        totals["bags_count"] = bags
        totals["destination"] = destination
        totals["origin"] = origin
        totals["total_price"] = bags * (float(trip[0]['bag_price']) + float(
            trip[1]['bag_price'])) + (float(trip[0]['base_price']) + float(trip[1]['base_price']))

        trip.append(totals)
        trip_list_totals.append(trip)
    logger.info("INFO: Total cost of flight calculated")

    return trip_list_totals
