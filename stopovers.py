"""
Search flights with Stopovers
Author: William A
April 2022
"""

import logging
import csv
import pprint

logging.basicConfig(level=logging.INFO, format="%(asctime)-15s %(message)s")
logger = logging.getLogger()


def search_stopovers(origin, destination, bags, csv_file):
    """Search connecting flights, outputs a list of possible flights with stopovers"""
    logger.info("INFO: Searching for Connections")

    flights_results = {
        "flights": [],
        "bags_allowed": 0,
        "bags_count": 0,
        "destination": 0,
        "origin": "",
        "total_price": 0,
        "travel_time": ""
    }
    with open(csv_file, newline='', encoding="utf-8") as flights_data:
        flights_reader = list(csv.DictReader(flights_data))

        for flight_row in flights_reader:

            if origin == flight_row['origin'] and destination != flight_row['destination']:
                if (bags >= 0) and bags <= int(flight_row['bags_allowed']):

                    flights_results['flights'].append(flight_row)

                    flight_new_origin = flight_row['destination']
                    for flight_row_conn in flights_reader:

                        if flight_new_origin == flight_row_conn[
                                'origin'] and destination == flight_row_conn['destination']:
                            logger.info("INFO: Connection flight found")

                            if (bags >= 0) and bags <= int(
                                    flight_row_conn['bags_allowed']):

                                flights_results['flights'].append(
                                    flight_row_conn)
                                logger.info("INFO: Adding connections Route")

                            else:
                                logger.info(
                                    "ERROR: Unauthorized number of bags  in one of the connection flights")

                else:
                    logger.info("ERROR: Unauthorized number of bags ")

    if len(flights_results["flights"]) == 0:
        logger.info("INFO: No flights connections found")

    else:
        logger.info("SUCCESS: Flights connections found")

    return flights_results
