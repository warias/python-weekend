"""
Search nonstop flights
Author: William A
April 2022
"""

import logging
import csv
import pprint

logging.basicConfig(level=logging.INFO, format="%(asctime)-15s %(message)s")
logger = logging.getLogger()


def search_nonstop(origin, destination, bags, csv_file):
    """Search direct flights, when the origin and destination are in the
    same row from the given dataset"""
    search_results = []
    with open(csv_file, newline='', encoding="utf-8") as flights_data:
        flights_reader = csv.DictReader(flights_data)

        for flight_row in flights_reader:

            flights_results = {
                "flights": [],
                "bags_allowed": 0,
                "bags_count": 0,
                "destination": 0,
                "origin": "",
                "total_price": 0,
                "travel_time": ""
            }
            if origin == flight_row['origin'] and destination == flight_row['destination']:
                logger.info("INFO: Found direct flight")
                if (bags >= 0) and bags <= int(flight_row['bags_allowed']):
                    logger.info("INFO: Direct Flight Bags policy OK")
                    flights_results['flights'].append(flight_row)
                    flights_results['bags_allowed'] = flight_row['bags_allowed']
                    flights_results['origin'] = flight_row['origin']
                    flights_results['destination'] = flight_row['destination']
                    flights_results['bags_count'] = bags
                    flights_results['total_price'] = bags * \
                        (float(flight_row['bag_price'])) + \
                        float(flight_row['base_price'])
                    search_results.append(flights_results)
                    logger.info(
                        "SUCCESS: Returning search results for Direct Flight")

                else:
                    logger.info(
                        "ERROR: Unauthorized number of bags  in the flight")

        if len(search_results) == 0:
            logger.info("INFO: No direct flights found")

        else:
            logger.info("SUCCESS: Direct flight found")

    return search_results
