# Python-weekend

### High Level components interactions

![diagram](images/diagram.png)


## How to execute these scripts

```
git clone git@gitlab.com:warias/python-weekend.git
cd python-weekend
python main.py data/example_test_conn.csv --origin DHE --destination NRX --bags=2

```

You should get this output:

```
2022-04-05 16:36:52,658 INFO: No direct flights found
2022-04-05 16:36:52,658 INFO: Searching for Connections
2022-04-05 16:36:52,658 INFO: Connection flight found
2022-04-05 16:36:52,658 INFO: Adding connections Route
2022-04-05 16:36:52,658 SUCCESS: Flights connections found
2022-04-05 16:36:52,659 INFO: Total cost of flight calculated
2022-04-05 16:36:52,659 SUCCESS: Generated json-compatible structured list with flights

[{'flights': [{'flight_no': 'WM478',
               'origin': 'DHE',
               'destination': 'NIZ',
               'departure': '2021-09-01T00:40:00',
               'arrival': '2021-09-01T05:50:00',
               'base_price': '248.0',
               'bag_price': '12',
               'bags_allowed': '2'},
              {'flight_no': 'WM478',
               'origin': 'NIZ',
               'destination': 'NRX',
               'departure': '2021-09-01T08:50:00',
               'arrival': '2021-09-01T14:00:00',
               'base_price': '248.0',
               'bag_price': '12',
               'bags_allowed': '2'}],
  'bags_allowed': 2,
  'bags_count': 2,
  'destination': 'NRX',
  'origin': 'DHE',
  'total_price': 544.0,
  'travel_time': ''}]

```
## Description
The `main.py` script currently accepts these arguments:

| Argument Name | Type | Description |
| ---      | ---      | ---      |
| `filename` | input **csv file** path   | Positional argument. Must be placed  first after `main.py` script   |
| `origin` | string   | Origin airport code   |
| `destination` | string |  Destination airport code |
| `bags`   |   int       | Number of bags  |

## Test and Deploy

This project use built-in continuous integration.

Example Execution from CI Pipeline [output](https://gitlab.com/warias/python-weekend/-/jobs/2294781350)


## Authors and acknowledgment
This project corresponds to [Python Weekend Task](https://github.com/kiwicom/python-weekend-entry-task)

## License
MIT

## Project status
Current arguments implemented are described in the table
